package mands.orderprocessing.orders;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

public class OrderManager {

    private final String ORDER_FILE_NAME = "data/Orders.xml";
    private static OrderManager ourInstance;
    private ArrayList<Order> orders;

    private OrderManager(){}

    public static OrderManager getInstance(){
        if (ourInstance == null) {
            ourInstance = new OrderManager();
        }
        return ourInstance;
    }

    public void loadData() throws DOMException, InvalidDataException, IOException, IllegalParameterException,
            ParserConfigurationException, SAXException {

        OrderReader or = new OrderXMLReader(ORDER_FILE_NAME);
        orders = or.loadData();
    }

    // Returns list of order Data Transfer Objects
    public ArrayList<OrderDTO> getDTOList() throws IllegalParameterException, InvalidDataException {

        ArrayList<OrderDTO> dtoList = new ArrayList<>();

        for (Order o : orders){
            OrderDTO dto = new OrderDTO(o.getOrderNumber(), o.getId(), o.getStartDay(),
                                        o.getDestination(), o.copyOrderItems());

            dtoList.add(dto);
        }
        return dtoList;
    }
}
