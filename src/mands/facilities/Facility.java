package mands.facilities;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

public interface Facility {

    void displayStatusReport();

    String getFacilityName();

    int getProductionRate();

    int getDailyProductionCost();

    //Schedule methods

    boolean hasDay(Integer dayIn) throws IllegalParameterException;

    public Integer getAvailability(Integer dayIn) throws IllegalParameterException;

    public void addDay(Integer dayIn) throws IllegalParameterException;

    public void decreaseAvailability(Integer dayIn, Integer valueIn) throws IllegalParameterException;

    //Inventory methods

    boolean hasItem(String idIn) throws InvalidDataException;

    int getItemQuantity(String id) throws InvalidDataException;

    void decreaseInventory(String id, Integer quantity) throws IllegalParameterException, InvalidDataException;
}
