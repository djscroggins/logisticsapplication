package mands.orderprocessing.facilityrecord;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

public class StandardFacilityRecord implements FacilityRecord {

    private String siteName;
    private int quantAvailable;
    private int endDay;
    private int travelTime;
    private Integer arrivalDay; // Must be Integer for compareTo()

    public StandardFacilityRecord(String siteNameIn, int quantAvailableIn, int endDayIn, int travelTimeIn, int arrivalDayIn) throws InvalidDataException, IllegalParameterException {
        setSiteName(siteNameIn);
        setQuantAvailable(quantAvailableIn);
        setEndDay(endDayIn);
        setTravelTime(travelTimeIn);
        setArrivalDay(arrivalDayIn);
    }

    private void setSiteName(String siteNameIn) throws InvalidDataException {
        if (siteNameIn == null || siteNameIn.isEmpty()){
            throw new InvalidDataException(siteNameIn == null ? "StandardFacilityRecord.setSiteName(string): string is null"
                    : "StandardFacilityRecord.setSiteName(string): string is empty");
        }
        siteName = siteNameIn;
    }

    private void setQuantAvailable(int quantAvailableIn) throws IllegalParameterException {
        if (quantAvailableIn <= 0){
            throw new IllegalParameterException("StandardFacilityRecord.setQuantAvailable(int): int must be >0; input:" + quantAvailableIn);
        }
        quantAvailable = quantAvailableIn;
    }

    private void setEndDay(int endDayIn) throws IllegalParameterException {
        if (endDayIn <= 0){
            throw new IllegalParameterException("StandardFacilityRecord.setEndDay(int): int must be >0; input:" + endDayIn);
        }
        endDay = endDayIn;
    }

    private void setTravelTime(int travelTimeIn) throws IllegalParameterException {
        if (travelTimeIn <= 0){
            throw new IllegalParameterException("StandardFacilityRecord.setTravelTime(int): int must be >0; input:" + travelTimeIn);
        }
        travelTime = travelTimeIn;
    }

    private void setArrivalDay(Integer arrivalDayIn) throws IllegalParameterException {
        if (arrivalDayIn <= 0){
            throw new IllegalParameterException("StandardFacilityRecord.setArrivalDay(Integer): Integer must be >0; input:" + arrivalDayIn);
        }
        arrivalDay = arrivalDayIn;
    }

    public String getSiteName() {
        return siteName;
    }

    public int getQuantAvailable() {
        return quantAvailable;
    }

    public int getTravelTime() {
        return travelTime;
    }

    @Override
    public int compareTo(FacilityRecord fr) {
        StandardFacilityRecord sfr = (StandardFacilityRecord) fr;
        int lastCmp = arrivalDay.compareTo(sfr.arrivalDay);
        return (lastCmp != 0 ? lastCmp : arrivalDay.compareTo(sfr.arrivalDay));
    }
}
