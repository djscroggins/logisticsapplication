package mands.orderprocessing.orders;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import mands.orderprocessing.orders.Order;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

public interface OrderReader {

    ArrayList<Order> loadData() throws DOMException, InvalidDataException, IOException,
            IllegalParameterException, ParserConfigurationException,
            SAXException;

}
