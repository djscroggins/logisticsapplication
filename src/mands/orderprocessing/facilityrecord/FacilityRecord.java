package mands.orderprocessing.facilityrecord;

public interface FacilityRecord extends Comparable<FacilityRecord> {

    String getSiteName();

    int getQuantAvailable();

    int getTravelTime();

}
