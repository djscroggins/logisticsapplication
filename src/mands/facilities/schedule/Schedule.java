package mands.facilities.schedule;

import mands.exceptions.IllegalParameterException;

public interface Schedule {

     void displaySchedule();

     boolean hasDay(Integer dayIn) throws IllegalParameterException;

     Integer getAvailability(Integer dayIn) throws IllegalParameterException;

     void addDay(Integer dayIn) throws IllegalParameterException;

     void decreaseAvailability(Integer dayIn, Integer valueIn) throws IllegalParameterException;

}
