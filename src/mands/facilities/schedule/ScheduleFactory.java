package mands.facilities.schedule;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

public class ScheduleFactory {

    public static Schedule createSchedule(Integer rate) throws IllegalParameterException, InvalidDataException
    {
        return new ScheduleImpl(rate);
    }

}
