package mands.orderprocessing.orders;

import java.util.TreeMap;

public interface Order {

    String getId();

    int getOrderNumber();

    int getStartDay();

    String getDestination();

    TreeMap<String, Integer> copyOrderItems();

}
