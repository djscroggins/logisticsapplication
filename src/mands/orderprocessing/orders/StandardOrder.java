package mands.orderprocessing.orders;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import mands.orderprocessing.orders.Order;

import java.util.TreeMap;

public class StandardOrder implements Order {

    private int orderNumber;
    private String id;
    private int startDay;
    private String destination;
    private TreeMap<String, Integer> orderItems;


    public StandardOrder(int orderNumberIn, String idIn, int startDayIn, String destinationIn, TreeMap<String, Integer> orderItemsIn) throws IllegalParameterException, InvalidDataException {
        setOrderNumber(orderNumberIn);
        setId(idIn);
        setStartDay(startDayIn);
        setDestination(destinationIn);
        setOrderItems(orderItemsIn);
    }

    // Counter for order in which orders were entered
    private void setOrderNumber(int orderNumberIn) throws IllegalParameterException {
        if (orderNumberIn <= 0){
            throw new IllegalParameterException("StandardOrder.setOrderNumber(int): Integer must be >0; input: " + orderNumberIn);
        }

        orderNumber = orderNumberIn;
    }

    private void setId(String idIn) throws InvalidDataException {
        if (idIn == null || idIn.isEmpty()){
            throw new InvalidDataException(idIn == null ? "StandardOrder.setId(string): string is null"
                    : "StandardOrder.setId(string): string is empty");
        }
        id = idIn;
    }

    private void setStartDay(int startDayIn) throws IllegalParameterException {
        if (startDayIn <= 0){
            throw new IllegalParameterException("StandardOrder.setStartDay(int): Integer must be >0; input: " + startDayIn);
        }
        startDay = startDayIn;
    }

    private void setDestination(String destinationIn) throws InvalidDataException {
        if (destinationIn == null || destinationIn.isEmpty()){
            throw new InvalidDataException(destinationIn == null ? "StandardOrder.setDestination(string): string is null"
                    : "StandardOrder.setDestination(string): string is empty");
        }
        destination = destinationIn;
    }

    private void setOrderItems(TreeMap<String, Integer> orderItemsIn) throws InvalidDataException, IllegalParameterException {
        if (orderItemsIn == null || orderItemsIn.isEmpty()){
            throw new InvalidDataException(orderItemsIn == null ? "FacilityImpl.setOrderItems(string): string is null"
                    : "FacilityImpl.setOrderItems(string): string is empty");
        }

        orderItems = orderItemsIn;
    }

    public String getId() {
        return id;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public int getStartDay() {
        return startDay;
    }

    public String getDestination() {
        return destination;
    }

    private TreeMap<String, Integer> getOrderItems() {
        return orderItems;
    }

    // Copies orderItems to ensure integrity of original order collection
    public TreeMap<String, Integer> copyOrderItems(){
        TreeMap<String, Integer> oldOrderItems = getOrderItems();
        TreeMap<String, Integer> newOrderItems = new TreeMap<>();

        for (String id : oldOrderItems.keySet()){
            newOrderItems.put(id, oldOrderItems.get(id));
        }

        return newOrderItems;
    }
}
