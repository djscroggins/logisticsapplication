package mands.orderprocessing.orders;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

public class OrderXMLReader implements OrderReader {

    private String fileName;
    private ArrayList<Order> objectManager = new ArrayList<>();

    public OrderXMLReader(String fileNameIn) {
        setFileName(fileNameIn);
    }

    private void setFileName(String fileNameIn) {
        fileName = fileNameIn;
    }

    private String getFileName() {
        return fileName;
    }

    public ArrayList<Order> loadData() throws DOMException, InvalidDataException, IOException,
            IllegalParameterException, ParserConfigurationException,
            SAXException{

        String fileName = getFileName();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        File xml = new File(fileName);
        if (!xml.exists()) {
            System.err.println("**** XML File '" + fileName + "' cannot be found");
            System.exit(-1);
        }

        Document doc = db.parse(xml);
        doc.getDocumentElement().normalize();

        NodeList orderEntries = doc.getDocumentElement().getChildNodes();

        int orderNumber = 1;

        for (int i = 0; i < orderEntries.getLength(); i++) {

            if (orderEntries.item(i).getNodeType() == Node.TEXT_NODE) {
                continue;
            }

            String entryName = orderEntries.item(i).getNodeName();
            if (!entryName.equals("Order")) {
                System.err.println("Unexpected node found: " + entryName);
            }

            // Get nodes for Id, Start, Destination
            NamedNodeMap aMap = orderEntries.item(i).getAttributes();
            String orderId = aMap.getNamedItem("Id").getNodeValue();
            Element elem = (Element) orderEntries.item(i);
            int start = Integer.parseInt(elem.getElementsByTagName("Start").item(0).getTextContent());
            String destination = elem.getElementsByTagName("Destination").item(0).getTextContent();

            // Get nodes for Items; Store in Tree Map
            TreeMap<String, Integer> orderItems = new TreeMap<>();
            NodeList itemList = elem.getElementsByTagName("Item");

            // Generate TreeMap for Items
            for (int j = 0; j < itemList.getLength(); j++) {
                if (itemList.item(j).getNodeType() == Node.TEXT_NODE) {
                    continue;
                }

                entryName = itemList.item(j).getNodeName();
                if (!entryName.equals("Item")) {
                    System.err.println("Unexpected node found: " + entryName);
                    //return;
                }

                elem = (Element) itemList.item(j);
                String itemId = elem.getElementsByTagName("ID").item(0).getTextContent();
                Integer itemQuant = Integer.parseInt(elem.getElementsByTagName("Quantity").item(0).getTextContent());
                orderItems.put(itemId, itemQuant);

            }

            // Create Order object and store in ArrayList; returned to OrderManager
            Order o = OrderFactory.createOrder(orderNumber, orderId, start, destination, orderItems);
            objectManager.add(o);
            orderNumber +=1;

        }

        return objectManager;
    }
}
