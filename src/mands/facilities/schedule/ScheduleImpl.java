package mands.facilities.schedule;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

import java.io.StringWriter;
import java.util.TreeMap;

public class ScheduleImpl implements Schedule{

    private Integer rate;
    private TreeMap<Integer, Integer> scheduleTMap;

    public ScheduleImpl(Integer rateIn) throws InvalidDataException, IllegalParameterException {

        setRate(rateIn);
        scheduleTMap = new TreeMap<>();
        initializeSchedule();

    }

    //Schedule mutators
    private void setRate(Integer rateIn) throws IllegalParameterException {
        if (rateIn <= 0){
           throw new IllegalParameterException("ScheduleImpl.setRate(rate): rate must be >0; input: " + rateIn);
        }
        rate = rateIn;
    }

    //Initializes empty schedule for 20 days at default rate
    private void initializeSchedule() throws InvalidDataException {
        if (scheduleTMap.isEmpty()) {
            for (int i = 1; i < 21; i++){
                scheduleTMap.put(i, rate);
            }
        }
    }

    //Schedule accessors
    public boolean hasDay(Integer dayIn) throws IllegalParameterException {
        if (dayIn <= 0){
            throw new IllegalParameterException("ScheduleImpl.hasDay(day): day must be >0; input: " + dayIn);
        }
        return scheduleTMap.containsKey(dayIn);
    }

    public Integer getAvailability(Integer dayIn) throws IllegalParameterException {
        if (dayIn <= 0) {
            throw new IllegalParameterException("ScheduleImpl.getAvailability(day): day must be >0; input: " + dayIn);
        }
        return scheduleTMap.get(dayIn);
    }

    public void addDay(Integer dayIn) throws IllegalParameterException {
        if (dayIn <= 0) {
            throw new IllegalParameterException("ScheduleImpl.addDay(day): day must be >0; input: " + dayIn);
        }
        scheduleTMap.put(dayIn, rate);
        }

    //Schedule mutators

    public void decreaseAvailability(Integer dayIn, Integer valueIn) throws IllegalParameterException {
        if (dayIn <= 0 || valueIn < 0){
            throw  new IllegalParameterException(dayIn <= 0 ? "ScheduleImpl.decreaseAvailability(day): day must be > 0; input: "
                    + dayIn : "ScheduleImpl.decreaseAvailability(value): value must be non-negative: value: " + valueIn);
        }
        scheduleTMap.put(dayIn, scheduleTMap.get(dayIn) - valueIn);
    }

    public void displaySchedule() {
        System.out.println("\nSchedule:");

        StringWriter dayString = new StringWriter();
        StringWriter availableString = new StringWriter();

        for (Integer day: scheduleTMap.keySet()){

            String key = day.toString();
            dayString.append("\t").append(key);

            String value = scheduleTMap.get(day).toString();
            availableString.append("\t").append(value);
        }
        System.out.println("Day:\t\t" + dayString);
        System.out.println("Available:\t" + availableString);
    }
}
