# logisticsApplication

This is a project I completed for an Object-Oriented program course while a student at DePaul University.

The application processes orders and then moves them from one or more facilities to a destination factility.

The items are processed at the source facility, loaded then transported via selected shipping routes.

The application uses an implementation of Dijkstra's algorithm to calculate the shortest shipping route between facilities.

Order processing requires the calculation of the shortest travel distances, 
processing time as well as travel time to determine from which facility an order 
should be filled.

Each facility has unique scheduling and processing capabilities to determine how 
quickly they can process an order. Additionally, each facility maintains and inventory
to track available items which are cross-referenced with a catalog to determine processing
cost.

For purposes of the project we used structured XML files to load the data. 

When run the application calculates and displays a summary of processing all inputted orders
with indication of back orders placed based on current inventory.

The current structure was built inside IntelliJ so will easily run there, but can be easily 
imported to other IDEs. When I have the time I might type up a quick build script.


