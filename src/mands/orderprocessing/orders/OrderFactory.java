package mands.orderprocessing.orders;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

import java.util.TreeMap;

public class OrderFactory {
    public static Order createOrder(int orderNumber, String id, int startDay, String destination,
                                    TreeMap<String, Integer> orderItems) throws IllegalParameterException, InvalidDataException{
        return new StandardOrder(orderNumber, id, startDay, destination, orderItems);
    }
}
