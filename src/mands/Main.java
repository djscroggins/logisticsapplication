package mands;

import mands.catalog.CatalogManager;
import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import mands.facilities.FacilityManager;
import mands.orderprocessing.OrderProcessor;
import mands.orderprocessing.orders.OrderManager;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        //Load Catalog first so FacilityManager can validate xml inventory items
        try {
            CatalogManager.getInstance().loadData();
        } catch (DOMException | IllegalParameterException | IOException | InvalidDataException |
                ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }

        //Load and display facilities network
        try {
            FacilityManager.getInstance().loadData();
            FacilityManager.getInstance().displayStatusReports();
        } catch (DOMException | IllegalParameterException | IOException | InvalidDataException
                | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }

        System.out.println("-----------------------------------------------------------------------------------------------------");

        //Load orders
        try {
            OrderManager.getInstance().loadData();
        } catch (DOMException | IllegalParameterException | IOException | InvalidDataException |
                ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }

        //Process orders
        try {
            OrderProcessor.getInstance().processOrders();
        } catch (InvalidDataException | IllegalParameterException e) {
            e.printStackTrace();
        }

        //Display final facility status reports
        FacilityManager.getInstance().displayStatusReports();

    }
}
