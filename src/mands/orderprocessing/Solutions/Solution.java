package mands.orderprocessing.Solutions;

public interface Solution {

    String getFacilityName();

    int getQuantity();

    double getCost();

    int getArrivalDay();

}
