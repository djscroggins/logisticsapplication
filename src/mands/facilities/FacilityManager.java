package mands.facilities;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

public class FacilityManager {

    private final String FACILITY_FILE_NAME = "data/FacilityXML.xml";
    private static FacilityManager ourInstance;
    private TreeMap<String, Facility> objectManager;

    private FacilityManager(){}

    public static FacilityManager getInstance(){
        if (ourInstance == null){
            ourInstance = new FacilityManager();
        }
        return ourInstance;
    }

    public void loadData() throws DOMException, InvalidDataException, IOException, IllegalParameterException,
                                 ParserConfigurationException, SAXException {

        FacilityReader fr = new FacilityXMLReader(FACILITY_FILE_NAME);
        objectManager = fr.loadData();
    }

    //Facility accessors

    public int getFacilityRate(String nameIn) throws InvalidDataException {
        if (nameIn == null || nameIn.isEmpty()){
            throw new InvalidDataException(nameIn == null ? "FacilityManager.getInstance.getFacilityRate(string); string is null"
                    : "FacilityManager.getInstance.getFacilityRate(string); string is empty");
        }
        return objectManager.get(nameIn).getProductionRate();
    }

    public int getFacilityCost(String nameIn) throws InvalidDataException {
        if (nameIn == null || nameIn.isEmpty()){
            throw new InvalidDataException(nameIn == null ? "FacilityManager.getInstance.getFacilityCost(string); string is null"
                    : "FacilityManager.getInstance.getFacilityCost(string); string is empty");
        }
        return objectManager.get(nameIn).getDailyProductionCost();
    }

    public void displayStatusReports() {
        for (String name: objectManager.keySet()){
            objectManager.get(name).displayStatusReport();
        }
    }


    /* Returns arrayList of facility names. In lieu of facility DTOs since facility content is updated in
    OrderProcessor and queries need to be current. DTO would not have reduced the need for accessors.*/
    public ArrayList<String> getFacilityList(){

        ArrayList<String> facilityList = new ArrayList<>();

        for (String key : objectManager.keySet()){
            facilityList.add(key);
        }
        return facilityList;
    }

    //Schedule accessors

    // Takes facility id string and desired day; returns 1 if day is in schedule etc.
    public boolean hasDay(String facilityIn, Integer dayIn) throws InvalidDataException, IllegalParameterException {
        if (facilityIn == null || facilityIn.isEmpty()){
            throw new InvalidDataException(facilityIn == null ? "FacilityManager.getInstance.hasDay(string, integer); string is null"
                    : "FacilityManager.getInstance.hasDay(string, integer); string is empty");
        }
        return objectManager.get(facilityIn).hasDay(dayIn);
    }

    // Takes facility id and desired day; returns amount of production slots open for day
    public Integer getAvailability(String facilityIn, Integer dayIn) throws InvalidDataException, IllegalParameterException {
        if (facilityIn == null || facilityIn.isEmpty()){
            throw new InvalidDataException(facilityIn == null ? "FacilityManager.getInstance.getAvailability(string, integer); string is null"
                    : "FacilityManager.getInstance.getAvailability(string, integer); string is empty");
        }
        return objectManager.get(facilityIn).getAvailability(dayIn);
    }

    //Schedule mutators

    public void addDay(String facilityIn, Integer dayIn) throws InvalidDataException, IllegalParameterException{
        if (facilityIn == null || facilityIn.isEmpty()){
            throw new InvalidDataException(facilityIn == null ? "FacilityManager.getInstance.addDay(string, integer); string is null"
                    : "FacilityManager.getInstance.addDay(string, integer); string is empty");
        }
        objectManager.get(facilityIn).addDay(dayIn);
    }

    // Takes facility id, desired day and value; decreases production slots for given day by value
    public void decreaseAvailability(String facilityIn, Integer dayIn, Integer valueIn) throws InvalidDataException, IllegalParameterException{
        if (facilityIn == null || facilityIn.isEmpty()){
            throw new InvalidDataException(facilityIn == null ? "FacilityManager.getInstance.decreaseAvailablity(string, integer, integer); string is null"
                    : "FacilityManager.getInstance.decreaseAvailablity(string, integer, integer); string is empty");
        }
        objectManager.get(facilityIn).decreaseAvailability(dayIn, valueIn);
    }

    //Inventory accessors

    public boolean hasItem(String facilityIn, String itemIn) throws InvalidDataException {
        if (facilityIn == null || facilityIn.isEmpty()){
            throw new InvalidDataException(facilityIn == null ? "FacilityManager.getInstance.hasItem(string1, string2); string1 is null"
                                                              : "FacilityManager.getInstance.hasItem(string1, string2); string1 is empty");
        }
        return objectManager.get(facilityIn).hasItem(itemIn);
    }

    public int getItemQuantity(String facilityIn, String itemIn) throws InvalidDataException {
        if (facilityIn == null || facilityIn.isEmpty()){
            throw new InvalidDataException(facilityIn == null ? "FacilityManager.getInstance.getItemQuantity(string1, string2); string1 is null"
                                                              : "FacilityManager.getInstance.getItemQuantity(string1, string2); string1 is empty");
        }

        return objectManager.get(facilityIn).getItemQuantity(itemIn);
    }

    // Inventory mutators

    public void decreaseInventory(String nameIn, String itemIn, Integer valueIn) throws IllegalParameterException, InvalidDataException {
        if (nameIn == null || nameIn.isEmpty()){
            throw new InvalidDataException(nameIn == null ? "FacilityManager.getInstance.decreaseInventory(string, string, integer); string is null"
                    : "FacilityManager.getInstance.decreaseInventory(string, string, integer); string is empty");
        }
        objectManager.get(nameIn).decreaseInventory(itemIn, valueIn);
    }
}




