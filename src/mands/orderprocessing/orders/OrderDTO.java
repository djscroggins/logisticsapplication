package mands.orderprocessing.orders;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

import java.util.TreeMap;

public class OrderDTO {

    private int orderNumber;
    private String id;
    private int startDay;
    private String destination;
    private TreeMap<String, Integer> orderItems;

    public OrderDTO(int orderNumberIn, String idIn, int startDayIn, String destinationIn, TreeMap<String, Integer> orderItemsIn)
                                                                throws IllegalParameterException, InvalidDataException {
        setOrderNumber(orderNumberIn);
        setId(idIn);
        setStartDay(startDayIn);
        setDestination(destinationIn);
        setOrderItems(orderItemsIn);
    }

    // TODO might want to remove this validation as redundant
    private void setOrderNumber(int orderNumberIn) throws IllegalParameterException {
        if (orderNumberIn <= 0){
            throw new IllegalParameterException("OrderDTO.setOrderNumber(int): order number must be >0. int = " + orderNumberIn);
        }
        orderNumber = orderNumberIn;
    }

    private void setId(String idIn) throws InvalidDataException {
        if (idIn == null || idIn.isEmpty()){
            throw new InvalidDataException(idIn == null ? "OrderDTO.setId(string); string is null"
                    : "OrderDTO.setId(string); string is empty");
        }
        id = idIn;
    }

    private void setStartDay(int startDayIn) throws IllegalParameterException {
        if (startDayIn <= 0){
            throw new IllegalParameterException("OrderDTO.setStartDay(int): order number must be >0. int = " + startDayIn);
        }
        startDay = startDayIn;
    }

    private void setDestination(String destinationIn) throws InvalidDataException {
        if (destinationIn == null || destinationIn.isEmpty()){
            throw new InvalidDataException(destinationIn == null ? "OrderDTO.setDestination(string); string is null"
                    : "OrderDTO.setDestination(string); string is empty");
        }
        destination = destinationIn;
    }

    private void setOrderItems(TreeMap<String, Integer> orderItemsIn) throws InvalidDataException {
        if (orderItemsIn == null || orderItemsIn.isEmpty()){
            throw new InvalidDataException(orderItemsIn == null ? "OrderDTO.setOrderItems(collection); collection is null"
                    : "OrderDTO.setOrderItems(collection); collection is empty");
        }
        orderItems = orderItemsIn;
    }

    public String getId() {
        return id;
    }

    public int getStartDay() {
        return startDay;
    }

    public String getDestination() {
        return destination;
    }

    // TreeMap is copy of original TreeMap in StandardOrder
    public TreeMap<String, Integer> getOrderItems() {
        return orderItems;
    }

    public void displayDTO(){

        System.out.println("Order #" + orderNumber);
        System.out.println("\tOrder Id:\t\t" + id);
        System.out.println("\tOrder Time:\t\tDay " + startDay);
        System.out.println("\tDestination:\t" + destination);
        System.out.println("\n\tList of Order Items:");
        int counter = 1;
        for (String id: orderItems.keySet()){
            Integer quantity = orderItems.get(id);
            System.out.print("\t\t" + counter + ") Item ID:\t\t" + id + ",\t\tQuantity: " + quantity + "\n");
            counter += 1;
        }
        System.out.println("\n");

    }

}
