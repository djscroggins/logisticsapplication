package mands.orderprocessing.Solutions;

import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;

import java.text.NumberFormat;
import java.util.Locale;

public class StandardSolution implements Solution {

    private String facilityName;
    private int quantity;
    private double cost;
    private int arrivalDay;

    public StandardSolution(String facilityNameIn, int quantityIn, double costIn, int arrivalDayIn) throws InvalidDataException, IllegalParameterException {
        setFacilityName(facilityNameIn);
        setQuantity(quantityIn);
        setCost(costIn);
        setArrivalDay(arrivalDayIn);
    }

    private void setFacilityName(String facilityNameIn) throws InvalidDataException {
        if (facilityNameIn == null || facilityNameIn.isEmpty()){
            throw new InvalidDataException(facilityNameIn == null ? "StandardSolution.setFacilityName(string): string is null"
                    : "StandardSolution.setFacilityName(string): string is empty");
        }
        facilityName = facilityNameIn;
    }

    private void setQuantity(int quantityIn) throws IllegalParameterException {
        if (quantityIn <= 0){
            throw new IllegalParameterException("StandardSolution.setQuantity(int): int must be >0; input:" + quantityIn);
        }
        quantity = quantityIn;
    }

    private void setCost(double costIn) throws IllegalParameterException {
        if (costIn <= 0){
            throw new IllegalParameterException("StandardSolution.setCost(double): double must be >0; input:" + costIn);
        }
        cost = costIn;
    }

    private void setArrivalDay(int arrivalDayIn) throws IllegalParameterException {
        if (arrivalDayIn <= 0){
            throw new IllegalParameterException("StandardSolution.setArrivalDay(int): int must be >0; input:" + arrivalDayIn);
        }
        arrivalDay = arrivalDayIn;
    }

    @Override
    public String toString(){
        NumberFormat f = NumberFormat.getCurrencyInstance(Locale.US);
        return String.format("%-18s\t\t\t%d\t\t\t\t%s\t\t\t\t\t%d", getFacilityName(), getQuantity(), f.format(getCost()), getArrivalDay());
    }

    public String getFacilityName() {
        return facilityName;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getCost() {
        return cost;
    }

    public int getArrivalDay() {
        return arrivalDay;
    }
}
