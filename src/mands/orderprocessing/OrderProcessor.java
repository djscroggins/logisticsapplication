package mands.orderprocessing;

import mands.catalog.CatalogManager;
import mands.exceptions.IllegalParameterException;
import mands.exceptions.InvalidDataException;
import mands.facilities.FacilityManager;
import mands.orderprocessing.Solutions.Solution;
import mands.orderprocessing.Solutions.StandardSolution;
import mands.orderprocessing.facilityrecord.FacilityRecord;
import mands.orderprocessing.facilityrecord.StandardFacilityRecord;
import mands.orderprocessing.orders.OrderDTO;
import mands.orderprocessing.orders.OrderManager;
import mands.shortestpath.shortestpathalgorithm.ShortestPathManager;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.TreeMap;

public class OrderProcessor {

    private static OrderProcessor ourInstance;
    private final double TRANSPORT_COST = 500.00;

    private OrderProcessor(){}

    public static OrderProcessor getInstance(){
        if (ourInstance == null) {
            ourInstance = new OrderProcessor();
        }
        return ourInstance;
    }

    // Only public method; all other methods are private methods used internally by processOrders.
    public void processOrders() throws InvalidDataException, IllegalParameterException {

        ArrayList<OrderDTO> ordersList = OrderManager.getInstance().getDTOList();
        if (ordersList == null || ordersList.isEmpty()){
            throw new InvalidDataException(ordersList == null ? "OrderProcess.processOrders(): ordersList is null"
                    : "OrderProcess.processOrders(): ordersList is empty");
        }

        for (OrderDTO order : ordersList){
            order.displayDTO();
            // Get collection of specific items in order
            TreeMap<String, Integer> itemsTree = order.getOrderItems();

            double totalOrderCost = 0.0;
            System.out.println("Processing Solution:\n");

            for (String item : itemsTree.keySet()){
                ArrayList<Solution> solutionList = new ArrayList<>();

                // List of facility names in facility network.
                ArrayList<String> facilityList = FacilityManager.getInstance().getFacilityList();
                // Check if valid catalog item
                if (CatalogManager.getInstance().isValidInvItem(item)){

                    int quantNeeded = itemsTree.get(item);
                    //Returns list of facility solutions for item in order; sorted by arrival day
                    ArrayList<FacilityRecord> sortedRecords = generateSolutions(quantNeeded, order, item, facilityList);

                    //Returns final solutions for filling order
                    solutionList = processRecords(solutionList, sortedRecords, quantNeeded, order, item);

                    displaySolutions(item, solutionList);
                    // Returns total solution cost for each item
                    totalOrderCost += displaySolutionSummary(solutionList);

                } else {System.out.println("We do not stock the following item: " + item);}
            }

            NumberFormat f = NumberFormat.getCurrencyInstance(Locale.US);
            System.out.println("\nTotal Cost:\t\t" + f.format(totalOrderCost));
            System.out.println("-----------------------------------------------------------------------------------------------------");
        }
    }

    private ArrayList<FacilityRecord> generateSolutions(int quantNeeded, OrderDTO order, String item,
                                                        ArrayList<String> facilityList) throws InvalidDataException,
                                                                                               IllegalParameterException {

       if (quantNeeded < 0){
           throw new IllegalParameterException("OrderProcessor.generateSolutions(int quantNeeded,...): quantNeeded must be non-negative; quantNeeded was " + quantNeeded);
       }
        if (order == null){
            throw new InvalidDataException("OrderProcessor.generateSolutions(...,OrderDTO order,...): order is null");
        }
        if (item == null || item.isEmpty()){
            throw new InvalidDataException(item == null ? "OrderProcessor.generateSolutions(...,string item,...): item is null"
                    : "OrderProcessor.generateSolutions(...,string item,...): item is empty");
        }
        if (facilityList == null || facilityList.isEmpty()){
            throw new InvalidDataException(facilityList == null ? "OrderProcessor.generateSolutions(...,ArrayList facilityList,...): facilityList is null"
                    : "OrderProcessor.generateSolutions(...,ArrayList facilityList,...): facilityList is empty");
        }

        ArrayList<FacilityRecord> recordsList = new ArrayList<>();

        for (String facility : facilityList){

            if (FacilityManager.getInstance().hasItem(facility, item) && !order.getDestination().equals(facility)){
                //Determine quantity available at facility
                int facilityQuant = FacilityManager.getInstance().getItemQuantity(facility, item);
                int quantAvailable = Math.min(facilityQuant, quantNeeded);
                // Determine shortest path in days from current facility to destination

                if (quantAvailable != 0) {
                    int travelTime = (int) Math.ceil(getTravelTime(facility, order.getDestination()));
                    // Determine processing end day
                    int endDay = calculateEndDay(order, quantAvailable, facility);
                    // Determine arrival day
                    int arrivalDay = endDay + travelTime;

                    recordsList.add(new StandardFacilityRecord(facility, quantAvailable, endDay, travelTime, arrivalDay));
                }
            }
        }

        Collections.sort(recordsList);

        return recordsList;
    }

    private float getTravelTime(String source, String target) throws InvalidDataException {
        if (source == null || source.isEmpty()){
            throw new InvalidDataException(source == null ? "OrderProcessor.getTravelTime(string1, string2): string1 is null"
                    : "OrderProcessor.getTravelTime(string1, string2): string1 is empty");
        }
        if (target == null || target.isEmpty()){
            throw new InvalidDataException(target == null ? "OrderProcessor.getTravelTime(string1, string2): string2 is null"
                    : "OrderProcessor.getTravelTime(string1, string2): string2 is empty");
        }
        ShortestPathManager.getInstance().getShortestPath(source, target);
        return ShortestPathManager.getInstance().getTravelDays();
    }

    private int calculateEndDay(OrderDTO order, int quantAvailable, String facility) throws InvalidDataException, IllegalParameterException {
        int currentDay = order.getStartDay();

        while (quantAvailable != 0){

            if (FacilityManager.getInstance().hasDay(facility, currentDay)){
                int available = FacilityManager.getInstance().getAvailability(facility, currentDay);
                int scheduled = Math.min(available, quantAvailable);
                quantAvailable -= scheduled;
                currentDay += 1;
            } else {
                FacilityManager.getInstance().addDay(facility, currentDay);
                int available = FacilityManager.getInstance().getAvailability(facility, currentDay);
                int scheduled = Math.min(available, quantAvailable);
                quantAvailable -= scheduled;
                currentDay += 1;
            }

        }
        // currentDay will always advance one day beyond end before loop terminates
        return currentDay - 1;
    }

    private ArrayList<Solution> processRecords(ArrayList<Solution> solutionList, ArrayList<FacilityRecord> facilityRecords,
                                               int quantNeeded, OrderDTO order, String item) throws InvalidDataException,
                                                                                                    IllegalParameterException {

        if (solutionList == null){
            throw new InvalidDataException("OrderProcessor.processRecords(ArrayList,...): solutionList is null");
        }
        if (facilityRecords == null || facilityRecords.isEmpty()){
            throw new InvalidDataException(facilityRecords == null ? "OrderProcessor.processRecords(...,ArrayList,...): facilityRecords is null"
                                            : "OrderProcessor.processRecords(...,ArrayList,...): facilityRecords is empty");
        }

        //Tracks total quantity taken of this item
        for (FacilityRecord record : facilityRecords){

            if (quantNeeded > 0) { // Prevent processing additional orders once order fulfilled
                String facilityName = record.getSiteName();
                //Returns the amount taken from Inventory
                int reductionQuant = reduceInventory(record, quantNeeded, item, facilityName);
                quantNeeded -= reductionQuant;
                int endDay = updateSchedule(order, reductionQuant, facilityName);
                // Calculate item cost
                double totalCost = calculateCost(item, facilityName, record, reductionQuant);
                int arrivalDay = endDay + record.getTravelTime();

                solutionList.add(new StandardSolution(facilityName, reductionQuant, totalCost, arrivalDay));
            }
        }
        // Generate back order
        if (quantNeeded > 0) {
            System.out.println(quantNeeded + " of " + item + " has/have been back ordered.\n");
        }

        return solutionList;
    }

    private int reduceInventory(FacilityRecord record, int quantNeeded, String item, String facilityName)
                                throws IllegalParameterException, InvalidDataException {

        if (record == null){
            throw new InvalidDataException("OrderProcessor.reduceInventory(FacilityRecord record,...): record is null");
        }

        int reductionQuant = Math.min(quantNeeded, record.getQuantAvailable());
        FacilityManager.getInstance().decreaseInventory(facilityName, item, reductionQuant);

        return reductionQuant;
    }

    private int updateSchedule(OrderDTO order, int reductionQuant, String facilityName) throws InvalidDataException,
                                                                                               IllegalParameterException {
        int currentDay = order.getStartDay();

        while (reductionQuant != 0) {

            int available = FacilityManager.getInstance().getAvailability(facilityName, currentDay);
            int toSchedule = Math.min(available, reductionQuant);
            if (available > 0) {
                FacilityManager.getInstance().decreaseAvailability(facilityName, currentDay, toSchedule);
            }
            reductionQuant -= toSchedule;
            currentDay += 1;
        }

        return currentDay - 1;
    }

    private double calculateCost(String item, String facilityName, FacilityRecord record, int reductionQuant) throws InvalidDataException {

        double itemCost = (double) CatalogManager.getInstance().getItemCost(item) * reductionQuant;
        // Calculate production cost
        double daysNeeded = (double) reductionQuant / FacilityManager.getInstance().getFacilityRate(facilityName);
        double productionCost = daysNeeded * FacilityManager.getInstance().getFacilityCost(facilityName);
        //Calculate travel cost;
        double travelCost = (double) record.getTravelTime() * TRANSPORT_COST;

        return itemCost + productionCost + travelCost;
    }

    private void displaySolutions(String item, ArrayList<Solution> solutionList) throws InvalidDataException {

        if (solutionList == null || solutionList.isEmpty()){
            throw new InvalidDataException(solutionList == null ? "OrderProcessor.displaySolutions(...,ArrayList solutionList): solutionList is null"
                    : "OrderProcessor.displaySolutions(...,ArrayList solutionList): solutionList is empty");
        }

        System.out.println(item + ":");
        System.out.println("\t\tFacility\t\t\t\t\t\tQuantity\t\tCost\t\t\t\t\t\tArrival Day");
        int counter = 1;
        for (Solution solution : solutionList){
            System.out.println("\t\t" + counter + ") " + solution.toString());
            counter += 1;
        }
    }

    private double displaySolutionSummary(ArrayList<Solution> solutionList){

        int totalQuant = 0;
        double totalCost = 0.0;
        int firstDay = Integer.MAX_VALUE;
        int lastDay = Integer.MIN_VALUE;
        for (Solution solution : solutionList){
            totalQuant += solution.getQuantity();
            totalCost += solution.getCost();
            if (solution.getArrivalDay() < firstDay){
                firstDay = solution.getArrivalDay();
            }
            if (solution.getArrivalDay() > lastDay){
                lastDay = solution.getArrivalDay();
            }
        }

        NumberFormat f = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println(String.format("%16s\t\t\t\t\t\t%d\t\t\t\t%s\t\t\t\t\t[%d - %d]\n", "TOTAL", totalQuant,
                                         f.format(totalCost), firstDay, lastDay));

        return totalCost;
    }
}
