package mands.facilities.inventory;

import mands.exceptions.IllegalParameterException;

public interface InvItem {

    String getId();

    int getQuantity();

    void decreaseInvItemQuant(Integer numberIn) throws IllegalParameterException;

}
